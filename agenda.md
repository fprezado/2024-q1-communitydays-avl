**SDV WG Q1/Community Days**

When:  19.03-20.03 

Registration:
Register [here](https://eclipsecon.regfox.com/sdv-community-day-graz-2024)

Venue: 
HELMUT LIST HALLE - see [website]( https://www.helmut-list-halle.com/ )

Address: 
Waagner-Biro-Straße 98a
8020 Graz, Österreich

Contacts: 
info@helmut-list-halle.com
t +43 316 58 42 60

**Day 1: March 19th**

Program runs in hybrid form: 14-17:30
Evening meet & greet

| Time | What | Presenter |
| ------ | ------ | ------ |
|  13-14 |Registration|       |
| 14-14:30 | Welcome and opening remarks| Local representative & EF SDV Team |
| 14:30-16 | [Hybrid] presentations - 1.5 hours | M Ortegon: Insurance Blueprint |
| 16-16:30 | Coffee Break - 0.5 hours | | 
| 16:30-17:30 | [Hybrid] presentations - 1 hour | | 
| 17:30-19 | [on site only] Meet & Greet Reception | |
 
**Day 2: March 20th**

Morning workshop (on site only) 9-13
Afternoon runs in hybrid form: 13-15:30


| Time | What | Presenter |
| ------ | ------ | ------ |
|  8-9 |Registration & Coffee|       |
| 9-11:30 | [on-site only] open collaboration | M Ortegon: Insurance Blueprint Workshop |
| 10:30-11  | Coffee Break | |
| 11:30-12 | [on-site only] open collaboration wrap up | | 
| 12-13 | Lunch break ||
| 13-14:30 | [Hybrid] presentations - 1.5  hour | |
| 14:30-15 | Coffee Break || 
| 15-15:30 | [Hybrid] Event wrap up and closing remarks || 
